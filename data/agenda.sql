﻿DROP DATABASE IF EXISTS agenda2023;
CREATE DATABASE agenda2023;
USE agenda2023;

CREATE TABLE cita(
id int,
nombre varchar(400),
lugar varchar(100),
fechaHora datetime,
duracion int,
descripcion text,
PRIMARY KEY(id)
);